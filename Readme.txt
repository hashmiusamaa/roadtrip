1. Download the apache jena fuseki server from here: http://www-us.apache.org/dist/jena/binaries/apache-jena-fuseki-2.4.1.zip

2. Unzip the file and run the fuseki-server.bat file.

3. After the intimation that the fuseki server is running, go to this address: http://localhost:3030

4. Go to manage datasets and create a new dataset with name - geoDataV3.

5. Upload datasets which are provided using the environment setup tutorial provided here (https://www.youtube.com/watch?v=p0h0Po7as1E&ab_channel=IshanDikshit).

6. Now, there will be successful information that datasets are uploaded without any error.

7. Unzip the app file and open index.html in any browser.

8. Give the destination and arrival locations and click Lets Go to see the results as shown here (https://www.youtube.com/watch?v=yj28tqnrfCo&ab_channel=IshanDikshit).

9. Code walk-through can be seen at https://www.youtube.com/watch?v=XbQyoUPiUjs&t=24s&ab_channel=IshanDikshit

	$(document).ready(function(){		
		$('#budget').val('6');
		$('#rating').val('0');
		console.log("budgetrating set");
	});
	var sourceLongLat = {lon:0, lat:0}
	var destinationLongLat = {lon:0, lat:0}
	var latlongarray = [];
	var map = new OpenLayers.Map("mapdiv");
	map.addLayer(new OpenLayers.Layer.OSM("OSM"));

	var featuress = [];
	
	var epsg4326 =  new OpenLayers.Projection("EPSG:4326"); //WGS 1984 projection
	var projectTo = map.getProjectionObject(); //The map projection (Spherical Mercator)
	lonLat = new OpenLayers.LonLat( "74.12", "31.55460" ).transform(epsg4326, projectTo);      
	zoom=7;
	map.setCenter (lonLat, zoom);
	
	
			
	init();			
	function init(){
		
		map.layers.forEach(function(layer) { 
			if (layer.name != 'OSM') { 
				map.removeLayer(layer);
			} 
		});
		featuress = [];
		sourceLongLat = {lon:0, lat:0}
		destinationLongLat = {lon:0, lat:0}
		latlongarray = [];
	}
	
	
  function getLongLatForSource(){
	if ($('#source').val() != $('#destination').val()){
	  if ($('#source').val().length > 3){
	  	init();
		$.ajax({
		url: "http://localhost:3030/geoDataV3/query?query=PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema%23> PREFIX road: <http://www.semanticweb.org/ishandikshit/ontologies/2016/9/untitled-ontology-11%23> PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns%23> SELECT ?lat ?lon WHERE {  ?node road:has_address ?address .  ?address road:latitude ?lat .  ?address road:longitude ?lon .  ?node road:name ?city .  filter( regex(str(?city), \""+$('#source').val()+",\" ))} LIMIT 1",
				type: "GET",
				success: function(data) {
						// console.log(data);
						if(data.results.bindings.length > 0){
							sourceLongLat.lat = data.results.bindings[0].lat.value;
							sourceLongLat.lon = data.results.bindings[0].lon.value;
							getLongLatForDestination();
						}else{
							alert('Source City Not Found, Aborting.');
						}
					},
				error: function(err){ alert(JSON.stringify(err));
				}
			});
		} else{
			alert("Please Enter Correct Source City");
		}
		}else{
			alert("Same Source and Destination.");
		}
  }
  function getLongLatForDestination(){
	if ($('#destination').val().length > 3){
	  	$.ajax({
	  	url: "http://localhost:3030/geoDataV3/query?query=PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema%23> PREFIX road: <http://www.semanticweb.org/ishandikshit/ontologies/2016/9/untitled-ontology-11%23> PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns%23> SELECT ?lat ?lon WHERE {  ?node road:has_address ?address .  ?address road:latitude ?lat .  ?address road:longitude ?lon .  ?node road:name ?city .  filter( regex(str(?city), \""+$('#destination').val()+",\" ))} LIMIT 1",
	           	type: "GET",
	           	success: function(data) { 
						// // console.log(data);
						if(data.results.bindings.length > 0){
							destinationLongLat.lat = data.results.bindings[0].lat.value;
							destinationLongLat.lon = data.results.bindings[0].lon.value;
							// // console.log('Calling getRoute.');
							getRoute();
						}else {
							alert('Destination City Not Found, Aborting.');
						}
	  				},
	  		 	error: function(err){ 
					alert(JSON.stringify(err));
				}
	        });	  
		} else{
			alert("Please Enter Correct Destination City");
		}
	}

  
  function getRoute(){
	// console.log('Executing Get Route.');
	var APIuri = "http://openls.geog.uni-heidelberg.de/route?api_key=ee0b8233adff52ce9fd6afc2a2859a28&start="+sourceLongLat.lon+","+sourceLongLat.lat+"&end="+destinationLongLat.lon+","+destinationLongLat.lat+"&via=&lang=en&distunit=KM&routepref=Car&weighting=Fastest&avoidAreas=&useTMC=false&noMotorways=false&noTollways=false&noUnpavedroads=false&noSteps=false&noFerries=false&instructions=false";
		$.get(APIuri, 
		function(data){
			// // alert("Making route.");
			arr = data.childNodes[0].childNodes[3].childNodes[1].childNodes[3].childNodes[1].childNodes;
			latlongarray = [];
			for(node in arr){
				if (arr[node].childNodes==undefined){continue;}
				if (node % 2 != 0) {
					latlong = arr[node].childNodes[0].textContent.split(" ");
					latlon = {lat: latlong[0], lon: latlong[1]};
					latlongarray.push(latlon);
				}
			}
			if(latlongarray.length > 0){
				// console.log('Display path called.');
				displayPath();
			}else{
				// // alert("No Route Found!");
			}				
		}
	);
  }

    
	function displayPath(){
	    var lonLat = new OpenLayers.LonLat( sourceLongLat.lon, sourceLongLat.lat ).transform(epsg4326, projectTo);	         	    
	    var zoom=10;
	    map.setCenter (lonLat, zoom);

	    var Path = new OpenLayers.Layer.Vector("Path"); 

	    map.addLayer(Path);                    
	    map.addControl(new OpenLayers.Control.DrawFeature(Path, OpenLayers.Handler.Path));                                     
	    points = [];
	    for (latlong in latlongarray){
	    	lalong = latlongarray[latlong];
			points.push(new OpenLayers.Geometry.Point(lalong.lat, lalong.lon).transform(new OpenLayers.Projection("EPSG:4326"), map.getProjectionObject()));
	    }

	    var way = new OpenLayers.Geometry.LineString(points);

	    var style = { 
	      strokeColor: '#0000ff', 
	      strokeOpacity: 0.2,
	      strokeWidth: 5
	    };

	    var lineFeature = new OpenLayers.Feature.Vector(way, null, style);
	    Path.addFeatures([lineFeature]);
		populateRequired();
    }
	
	
	function populateRequired(){
//		// alert("Populating Required");

		if($('#hotels').is(':checked')){
			// alert('Populating Hotels');
			var type=$('#hotels').val();
			// alert(type);
			featuress[type] = [];
			for (latlon in latlongarray){
				if(latlon % 4 == 0){
					var lat = latlongarray[latlon].lat;
					var lon = latlongarray[latlon].lon;
					var icon = "http://downloadicons.net/sites/default/files/hotel-icon-7505.png";
					getThing(icon, lat, lon, type, function(result){
//						// // console.log(result);
						for (feature in result){
							featuress[type].push(result[feature]);
						}
					});
				}
			}
			
		}
		
		if($('#waterfalls').is(':checked')){
			// alert('Populating Waterfalls');
			var type=$('#waterfalls').val();
			featuress[type] = [];
			for (latlon in latlongarray){
				if(latlon % 4 == 0){
					var lat = latlongarray[latlon].lat;
					var lon = latlongarray[latlon].lon;
					
					var icon = "http://image.flaticon.com/icons/png/128/119/119572.png";
					getThing(icon, lat, lon, type, function(result){
//						// console.log(result);
						for (feature in result){
							featuress[type].push(result[feature]);
						}
					});
				}
			}
			
		}
		
		if($('#attractions').is(':checked')){
			// alert('Populating Attractions');
			var type=$('#attractions').val();
			featuress[type] = [];
			for (latlon in latlongarray){
				if(latlon % 4 == 0){
					var lat = latlongarray[latlon].lat;
					var lon = latlongarray[latlon].lon;
					
					var icon = "https://cdn0.iconfinder.com/data/icons/citycons/150/Citycons_park-128.png";
					getThing(icon, lat, lon, type, function(result){	
//						// console.log(result);
						for (feature in result){
							featuress[type].push(result[feature]);
						}
					});
				}
			}
			
		}
		
		if($('#museums').is(':checked')){
			// alert('Populating Museums');
			var type=$('#museums').val();
			// alert(type);
			featuress[type] = [];
			for (latlon in latlongarray){
				if(latlon % 4 == 0){
					var lat = latlongarray[latlon].lat;
					var lon = latlongarray[latlon].lon;
					var icon = "http://visitromania.info.ro/wp-content/themes/visit-romania/images/icons/icon-round-blue-manmade-museum.svg";
					getThing(icon, lat, lon, type, function(result){
//						// console.log(result);
						for (feature in result){
							featuress[type].push(result[feature]);
						}
					});
				}
			}
			
		}
		
		if($('#informations').is(':checked')){
//			// alert('Populating Information');
			var type=$('#informations').val();
//			// alert(type);
			featuress[type] = [];
			for (latlon in latlongarray){
				if(latlon % 4 == 0){
					var lat = latlongarray[latlon].lat;
					var lon = latlongarray[latlon].lon;
					var icon = "http://www.lowcountryren.com/nestor/Documentation/ionicons-1.4.1/src/android-information.svg";
					getThing(icon, lat, lon, type, function(result){
//						// console.log(result);
						for (feature in result){
							featuress[type].push(result[feature]);
						}
					});
				}
			}	
		}
		
		if($('#arch').is(':checked')){
//			// alert('Populating Information');
			var type=$('#arch').val();
//			// alert(type);
			featuress[type] = [];
			for (latlon in latlongarray){
				if(latlon % 4 == 0){
					var lat = latlongarray[latlon].lat;
					var lon = latlongarray[latlon].lon;
					var icon = "https://cdn3.iconfinder.com/data/icons/world-monuments/137/WorldMonuments-14-512.png";
					getThing(icon, lat, lon, type, function(result){
//						// console.log(result);
						for (feature in result){
							featuress[type].push(result[feature]);
						}
					});
				}
			}	
		}
		
		
		if($('#vp').is(':checked')){
//			// alert('Populating Information');
			var type=$('#vp').val();
//			// alert(type);
			featuress[type] = [];
			for (latlon in latlongarray){
				if(latlon % 4 == 0){
					var lat = latlongarray[latlon].lat;
					var lon = latlongarray[latlon].lon;
					var icon = "http://image.flaticon.com/icons/png/128/171/171853.png";
					getThing(icon, lat, lon, type, function(result){
//						// console.log(result);
						for (feature in result){
							featuress[type].push(result[feature]);
						}
					});
				}
			}	
		}
		
		if($('#cs').is(':checked')){
//			// alert('Populating Information');
			var type=$('#cs').val();
//			// alert(type);
			featuress[type] = [];
			for (latlon in latlongarray){
				if(latlon % 4 == 0){
					var lat = latlongarray[latlon].lat;
					var lon = latlongarray[latlon].lon;
					var icon = "http://icons.iconarchive.com/icons/dapino/summer-blue/256/Tent-icon.png";
					getThing(icon, lat, lon, type, function(result){
//						// console.log(result);
						for (feature in result){
							featuress[type].push(result[feature]);
						}
					});
				}
			}	
		}
		
		if($('#ps').is(':checked')){
//			// alert('Populating Information');
			var type=$('#ps').val();
//			// alert(type);
			featuress[type] = [];
			for (latlon in latlongarray){
				if(latlon % 4 == 0){
					var lat = latlongarray[latlon].lat;
					var lon = latlongarray[latlon].lon;
					var icon = "https://cdn3.iconfinder.com/data/icons/eco-farm-color/100/EcoFarm-17-512.png";
					getThing(icon, lat, lon, type, function(result){
//						// console.log(result);
						for (feature in result){
							featuress[type].push(result[feature]);
						}
					});
				}
			}	
		}
		
		if($('#mot').is(':checked')){
//			// alert('Populating Information');
			var type=$('#mot').val();
//			// alert(type);
			featuress[type] = [];
			for (latlon in latlongarray){
				if(latlon % 4 == 0){
					var lat = latlongarray[latlon].lat;
					var lon = latlongarray[latlon].lon;
					var icon = "http://downloadicons.net/sites/default/files/hotel-icon-7505.png";
					getThing(icon, lat, lon, type, function(result){
//						// console.log(result);
						for (feature in result){
							featuress[type].push(result[feature]);
						}
					});
				}
			}	
		}
		
		if($('#park').is(':checked')){
//			// alert('Populating Information');
			var type=$('#park').val();
//			// alert(type);
			featuress[type] = [];
			for (latlon in latlongarray){
				if(latlon % 4 == 0){
					var lat = latlongarray[latlon].lat;
					var lon = latlongarray[latlon].lon;
					var icon = "https://cdn0.iconfinder.com/data/icons/citycons/150/Citycons_park-128.png";
					getThing(icon, lat, lon, type, function(result){
//						// console.log(result);
						for (feature in result){
							featuress[type].push(result[feature]);
						}
					});
				}
			}	
		}
		
		if($('#cas').is(':checked')){
//			// alert('Populating Information');
			var type=$('#cas').val();
//			// alert(type);
			featuress[type] = [];
			for (latlon in latlongarray){
				if(latlon % 4 == 0){
					var lat = latlongarray[latlon].lat;
					var lon = latlongarray[latlon].lon;
					var icon = "http://icons.iconseeker.com/png/fullsize/at-beach/sandcastle.png";
					getThing(icon, lat, lon, type, function(result){
//						// console.log(result);
						for (feature in result){
							featuress[type].push(result[feature]);
						}
					});
				}
			}	
		}
		
		if($('#res').is(':checked')){
//			// alert('Populating Information');
			var type=$('#res').val();
//			// alert(type);
			featuress[type] = [];
			for (latlon in latlongarray){
				if(latlon % 10 == 0){
					var lat = latlongarray[latlon].lat;
					var lon = latlongarray[latlon].lon;
					var icon = "http://buziosturismo.com/wp-content/uploads/eat-flat-1.png";
					getYelp(icon, lat, lon, type, function(result){
//						// console.log(result);
						for (feature in result){
							featuress[type].push(result[feature]);
						}
					});
				}
			}	
		}
		
	}
	
	function update(){
//		// console.log(type_value);
		var arr = [];
		for(type in featuress){
				for (feature in featuress[type])
					arr.push(featuress[type][feature]);	
		}

		var vectorLayer = new OpenLayers.Layer.Vector("Amen");
		for (feature in arr){
			// console.log(arr[feature]);
			vectorLayer.addFeatures(arr[feature]);
			console.log("ALAKA MALAKA");
		}
		map.addLayer(vectorLayer);
		var controls = {
			selector: new OpenLayers.Control.SelectFeature(vectorLayer, { onSelect: createPopup, onUnselect: destroyPopup })
		};

		map.addControl(controls['selector']);
		controls['selector'].activate();
	}
	

	
	function getThing(icon, lon, lat, type, callback){
		var uri = "http://localhost:3030/geoDataV3/?query=PREFIX%20xsd:%20%3Chttp://www.w3.org/2001/XMLSchema%23%3E%20PREFIX%20rdf:%20%3Chttp://www.w3.org/1999/02/22-rdf-syntax-ns%23%3E%20PREFIX%20road:%20%3Chttp://www.semanticweb.org/ishandikshit/ontologies/2016/9/untitled-ontology-11%23%3E%20SELECT%20distinct%20?name%20?desc%20?lat%20?lon%20WHERE%20{%20?node%20road:name%20?name%20.%20?node%20road:has_address%20?address%20.%20?node%20road:has_details%20?details%20.%20?details%20road:description%20?desc%20.%20?address%20road:latitude%20?lat%20.%20?address%20road:longitude%20?lon%20.%20filter(?lat%3E%22"+(parseFloat(lat)-0.2).toString()+"%22^^xsd:decimal)%20.%20filter(?lat%3C%22"+(parseFloat(lat)+0.2).toString()+"%22^^xsd:decimal)%20.%20filter(?lon%3E%22"+(parseFloat(lon)-0.2).toString()+"%22^^xsd:decimal)%20.%20filter(?lon%3C%22"+(parseFloat(lon)+0.2).toString()+"%22^^xsd:decimal)%20.%20filter(?desc=%22"+type+"%22)%20}";
		$.ajax({
	  	url: uri,
	           	type: "GET",
	           	success: function(data) {
						callback(populateMapIcon(data.results.bindings, icon, true));
	  				},
	  		 	error: function(err){ return false; }
	        });
	  }
	  
	function getYelp(icon, lon, lat, type, callback){
		var uri = "http://localhost:3030/geoDataV3/?query=PREFIX%20xsd:%20%3Chttp://www.w3.org/2001/XMLSchema%23%3E%20PREFIX%20rdf:%20%3Chttp://www.w3.org/1999/02/22-rdf-syntax-ns%23%3E%20PREFIX%20road:%20%3Chttp://www.semanticweb.org/ishandikshit/ontologies/2016/9/untitled-ontology-11%23%3E%20SELECT%20distinct%20?city%20?name%20?lat%20?lon%20?cuisine%20?budget%20?rating%20WHERE%20{%20?node%20road:name%20?name%20;%20rdf:type%20road:foodplace%20;%20road:has_address%20?address%20;%20road:has_details%20?detail%20;%20road:cuisine%20?cuisine%20;%20road:budget%20?budget%20.%20?detail%20road:ratings%20?rating%20;%20road:reviews%20?reviews%20.%20?address%20road:latitude%20?lat%20;%20road:longitude%20?lon%20;%20road:strretname%20?street%20;%20road:state%20?state%20;%20road:city%20?city%20.%20filter(?lat%3E%22"+(parseFloat(lat)-0.02).toString()+"%22^^xsd:decimal)%20.%20filter(?lat%3C%22"+(parseFloat(lat)+0.02).toString()+"%22^^xsd:decimal)%20.%20filter(?lon%3E%22"+(parseFloat(lon)-0.02).toString()+"%22^^xsd:decimal)%20.%20filter(?lon%3C%22"+(parseFloat(lon)+0.02).toString()+"%22^^xsd:decimal)%20.%20filter(?budget%3C%22"+$('#budget').val()+"%22^^xsd:decimal)%20.%20filter(?rating%3E%22"+$('#rating').val()+"%22^^xsd:decimal)%20.%20}";
		console.log(uri);
		$.ajax({
	  	url: uri,
	           	type: "GET",
	           	success: function(data) {
						console.log(data.results.bindings.length);
						callback(populateMapIcon(data.results.bindings, icon, false));
	  				},
	  		 	error: function(err){ return false; }
	        });
	  }
	  
	  function populateMapIcon(data, icon, isThing){
			arrayOfFeatures = [];
			  if (isThing){

				for(dat in data){
					var output = data[dat];
		//			// console.log(output);

					arrayOfFeatures.push(new OpenLayers.Feature.Vector(
							new OpenLayers.Geometry.Point(output.lon.value, output.lat.value)
							.transform(epsg4326, projectTo),
							{description:output.desc.value, name:output.name.value},
							{externalGraphic: icon, 
							graphicHeight: 25, graphicWidth: 21, graphicXOffset:-12, graphicYOffset:-25  })
							);
				}
				}else{
					for(dat in data){
					var output = data[dat];
		//			// console.log(output);

					arrayOfFeatures.push(new OpenLayers.Feature.Vector(
							new OpenLayers.Geometry.Point(output.lon.value, output.lat.value)
							.transform(epsg4326, projectTo),
							{cuisine:output.cuisine.value, name:output.name.value, rating: output.rating.value, budget: output.budget.value},
							{externalGraphic: icon, 
							graphicHeight: 25, graphicWidth: 21, graphicXOffset:-12, graphicYOffset:-25  })
							);
					
					
					}
				}
//		// console.log(arrayOfFeatures);
		return arrayOfFeatures;
	  
	 }
	 
	     function createPopup(feature) {
      feature.popup = new OpenLayers.Popup.FramedCloud("pop",
          feature.geometry.getBounds().getCenterLonLat(),
          null,
          '<h4>' + feature.attributes.name + '</h4><div class="markerContent">'+JSON.stringify(feature.attributes)+'</div>',
          null,
          true,
          function() { controls['selector'].unselectAll(); }
      );
      map.addPopup(feature.popup);
    }

    function destroyPopup(feature) {
      feature.popup.destroy();
      feature.popup = null;
    }

		
   